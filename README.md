# README

## Images

* [node](https://hub.docker.com/_/node)
* [mysql](https://hub.docker.com/_/mysql)
* [adminer](https://hub.docker.com/_/adminer/)

## Liens

* [Get Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [Débuter avec Docker](https://www.udemy.com/course/debuter-avec-docker/) -> Udemy : Gildas Quiniou

## Notes

```sh
docker run --rm bash echo Test
docker run --rm bash:3.2 echo Test
docker run --rm bash
docker run -ti --rm bash
```

Pour afficher la liste des images :

```sh
docker images
```

-t : Pseudo-TTY = Terminal (STDIN(clavier)+STDOUT(écran))
-i : Interactif
-rm : Supprime automatiquement un container en fin d'execution

Donne le nom de l'hôte :

```sh
uname -n
```

Afficher les processus actif :

```sh
docker ps
```

Lister les couches d'une images :

```sh
docker history <IMAGE ID>
```

Lister les actions docker :

```sh
docker <tab> <tab>
```

Lister les images docker :

```sh
docker images <tab> <tab>
```

Lister les images docker de 'type' bash :

```sh
docker images bash
```

Lister l'aide d'une commande docker :

```sh
docker <command> --help
```

Arrêter et redémarrer un container en gardant ses données :

```sh
docker run -ti --rm bash
echo OK > fic
cat fic
ls -l
exit // No more files, when container destroy
```

Avec le `--rm` les containers disparaissent.

1. les mofifications faites dans un container sont temporaires
2. Quand on utilise un `docker run` on crée toujours un nouveau container

```sh
docker run -ti bash
echo OK > fic
cat fic
ls -l
exit
docker start -ai <container_name>
cat fic // File found
```

## Volume

### Partie 1

Un volume permet de monter un fichier ou un repertoire de la machine hôte dans le container.

`pwd`: repertoire actuel, chemin courant

:ro : read-only

```sh
echo OK > fic
cat fic
docker run -ti --rm -v $(pwd)/fic:/text bash // source_sur_la_machine_hôte:emplacement_du_montage_dans_le_container
cat /text // tout roule, je lis le même fichier
echo OK >> fic /text // modification du fichier -> concatenation
cat /text // La modif est faite
exit
cat fic // fichier conforme à la modif !
```

### Partie 2

```sh
mkdir src
echo OK > src/fic
cat src/fic // OK
docker run --rm -ti -v $(pwd)/src:/src bash
cat /src/fic // OK
ls -l /sbin
exit

docker run --rm -ti -v $(pwd)/src:/sbin bash
ls -l /sbin
```

Les volumes managés :

```sh
docker volume create mes_donnees
docker volume ls
docker run --rm -ti -v mes_donnees/src:/sbin bash
echo OK > src/fic
exit
docker ps -a // Il est bien supprimé
docker run --rm -ti -v mes_donnees/src:/sbin bash // on le relance
cat src/fic // Il y a conservation !
exit
docker volume inspect mes_donnees // l\'addresse du volume est dans le mountpoint
cd /var/lib/docker/volumes/mes_donnees/_data/
ls -l // fic est la
cat fic // OK
cd - // revenir la ou on était
docker run --rm -ti -v mes_donnees:/sbin bash
ls /sbin/ // fic est la
exit
cd /var/lib/docker/volumes/mes_donnees/_data/
ls // fic est la
rm fic
ls -l // on le supprime
cd -
docker run --rm -ti -v mes_donnees:/sbin bash // Quand on monte un volume managé vide, le dossier de montage du container est transferé dans le volume managé, le contenu peut-etre lu indifferement
```

## Réseau

### Partie 1

nginx remplace apache ?

```sh
docker run --rm -p 80:80 nginx
ifconfig
netstat -nate // liste des ports déjà ouvert
docker ps
docker inspect <container_name>
docker inspect <image>
```

```sh
<ctrl - c>
```

Comment mapper des ports de communication entre un container et le monde exterieur.

### Partie 2

Les different type/pilote de reseau.

pilote -> drivers : besoin different

5 pilotes standard :

* none : rien ne sort ou ne rentre du reseau
* bridge : pilote par défaut, on peut le cloner (le plus commun)
* host : partage la même pile reseau que la machine hôte
* overlay
* mcvlan

```sh
// none

--network <type>
docker run -ti --rm --network none bash
ifconfig

// bridge

docker run -ti --rm bash    // dans deux containers
ifconfig                    // *2
ping <inet addr de l\'autre>            // *2

// Toujours bridge, sur l\'hote

ifconfig // recup inet du docker0

// Toujours bridge, sur un des deux containers

ping <inet addr de l\'hôte>

docker network create --driver=bridge mon_bridge // on obtient l\'id du reseau qu\'on vient de créer
docker network --help
docker network ls
docker network rm <ID ou nom>
docker run -ti --rm --network=mon_bridge --name=srv1 bash
docker run -ti --rm --network=mon_bridge --name=srv2 bash // dans un autre terminal
ifconfig // *2

ping <nom de l\'autre> // = ping <inet addr de l\'autre>

docker network connect mon_bridge srv2 // sur un troisième terminal
```

Docker gère un DNS interne por les réseaux bridge cloné.

## Détacher

```sh
docker run -d -p 80:80 nginx
docker ps -a
docker attach <id>
docker ps
```

## Attacher

```sh
docker ps -a
docker attach <id>
docker ps
```

## Rattacher puis détacher sans couper le container (plutôt rare)

```sh
docker run -ti -d -p 80:80 nginx
docker ps -a
docker attach <id>
<ctrl - p / ctrl - q>
docker ps
```

---

```sh
docker stop <name>
docker ps -a
```

```sh
tail -f <filename>
```

## Créer son image

`exec` permet d'executer une commande dans un cvontainer déjà actif.

```sh
docker run -ti php
new SoapClient(); // error
```

Dans un autre terminal :

```sh
docker ps
docker exec -ti <id> bash
docker-php-ext-install <extension> // error: lib missing
apt update
apt-cache search libxml2
apt install libxml2-dev
docker-php-ext-install soap // install complete !
php -a
new SoapClient();
```

```sh
docker build -t php_soap:7.0.31 .
docker run -ti php_soap:7.0.31
new SoapClient('<address>');

docker build -t php_soap:7.2.8 .
docker run -ti php_soap:7.2.8
new SoapClient('<address>');
```

-t : Ajoute un tag.
-f : Si le dockerfile porte un autre nom, on doit lui indiquer.

Le point c'est pour indiquer le repertoire courrant.

## Docker-compose

> Chef d'orchestre.

```sh
docker-compose up
http://localhost:1080
<ctrl c>
docker-compose up -d // détaché
docker-compose up -f // logs
docker-compose stop
docker ps -a
docker-compose start
docker-compose down // Supprime tout sauf les volumes
```

Wordpress est installé dans /var/www/html

Pour la solution managé :

```sh
docker volume ls
```

`docker down` ne touche pas les volumes. Pour se faire : `docker down -v`.
